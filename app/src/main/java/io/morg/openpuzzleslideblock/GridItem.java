package io.morg.openpuzzleslideblock;

import android.graphics.Bitmap;

public class GridItem {

    int i;
    Bitmap b;

    public GridItem(int i, Bitmap b) {
        this.i = i;
        this.b = b;
    }

    public int getI() {
        return i;
    }

    public Bitmap getB() {
        return b;
    }

}
