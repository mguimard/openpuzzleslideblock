package io.morg.openpuzzleslideblock;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class GridItemAdapter extends ArrayAdapter<GridItem> {

    private final int free_index;
    private boolean shouldReveal = false;

    public GridItemAdapter(@NonNull Context context, ArrayList<GridItem> list, int free_index) {
        super(context, 0, list);
        this.free_index = free_index;
    }

    public void reveal(){
        shouldReveal = true;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            // Layout Inflater inflates each item to be displayed in GridView.
            v = LayoutInflater.from(getContext()).inflate(R.layout.grid_item, parent, false);
        }

        GridItem item  = getItem(position);
        GridItemView tv = v.findViewById(R.id.bitmap);

        if(shouldReveal) {
            tv.setImageBitmap(item.getB());
        }
        else if(item.getI() == free_index)
        {
            Bitmap black = Bitmap.createBitmap(item.getB().getWidth(), item.getB().getHeight(), Bitmap.Config.ARGB_8888);
            tv.setImageBitmap(black);
        }
        else
        {
            tv.setImageBitmap(item.getB());
        }

        if(shouldReveal){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 0, 0, 0);
            tv.setLayoutParams(params);
        }

        return v;
    }

}
