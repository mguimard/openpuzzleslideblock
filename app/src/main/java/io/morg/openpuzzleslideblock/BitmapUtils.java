package io.morg.openpuzzleslideblock;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class BitmapUtils {

    public static Bitmap rotate(Bitmap bitmap, int angle) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, width, height, true);
        return Bitmap.createBitmap(scaled, 0, 0, scaled.getWidth(), scaled.getHeight(), matrix, true);
    }

    public static Bitmap cropSquare(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        if (height > width) {
            return Bitmap.createBitmap(bitmap, 0, height / 2 - width / 2, width, width);
        } else {
            return Bitmap.createBitmap(bitmap, width / 2 - height / 2, 0, height, height);
        }
    }

    public static Bitmap[] split(Bitmap bitmap, int w, int h) {
        Bitmap[] bitmaps = new Bitmap[w*h];

        int width = bitmap.getWidth() / w;
        int height = bitmap.getHeight() / h;

        for(int y = 0; y < h; ++y) {
            for(int x = 0; x < w; ++x) {
                bitmaps[y * h + x] = Bitmap.createBitmap(bitmap, x * width, y * height, width, height);
            }
        }

        return bitmaps;
    }
}
