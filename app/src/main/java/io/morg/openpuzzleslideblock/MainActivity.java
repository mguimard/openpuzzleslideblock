package io.morg.openpuzzleslideblock;

import static android.view.MotionEvent.ACTION_UP;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    public static final int PICK_IMAGE_REQUEST_CODE = 1;
    public static final int MIN_GRID_SIZE = 3;
    public static final int MAX_GRID_SIZE = 7;

    private Uri current_uri;
    private int current_grid_size = MIN_GRID_SIZE;
    private int angle = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.files).setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE);
        });

        findViewById(R.id.decrease_grid_size).setOnClickListener(view -> {
            if (current_grid_size > MIN_GRID_SIZE) {
                current_grid_size--;
                load_game(current_uri);
            }
        });

        findViewById(R.id.increase_grid_size).setOnClickListener(view -> {
            if (current_grid_size < MAX_GRID_SIZE) {
                current_grid_size++;
                load_game(current_uri);
            }
        });

        findViewById(R.id.rotate).setOnClickListener(view -> {
            angle += 90;
            if(angle == 360) {
                angle = 0;
            }
            load_game(current_uri);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST_CODE) {
            current_uri = data.getData();
            load_game(current_uri);
        }
    }

    private boolean isSolvable(ArrayList<GridItem> data) {
        int count = 0;

        for (int i = 0; i < data.size() - 1; i++) {
            for (int j = i + 1; j < data.size(); j++) {
                if (data.get(i).getI() != data.size() - 1 && data.get(j).getI() != data.size() -1 && data.get(i).getI() > data.get(j).getI())
                    count++;
            }
        }

        return count % 2 == 0;
    }

    private void load_game(Uri uri) {

        if (uri == null) {
            return;
        }

        try {
            // Generate bitmaps
            Bitmap bitmap = BitmapUtils.cropSquare(MediaStore.Images.Media.getBitmap(getContentResolver(), uri));

            if(angle != 0) {
                bitmap = BitmapUtils.rotate(bitmap, angle);
            }

            Bitmap[] parts = BitmapUtils.split(bitmap, current_grid_size, current_grid_size);

            GridView grid_view = findViewById(R.id.grid);
            grid_view.setNumColumns(current_grid_size);

            ArrayList<GridItem> list = new ArrayList<>();

            for (int y = 0; y < current_grid_size; y++) {
                for (int x = 0; x < current_grid_size; x++) {
                    int index = y * current_grid_size + x;
                    list.add(new GridItem(index, parts[index]));
                }
            }

            // Make sure puzzle is solvable
            // int n_iterations = 0;

            do {
                Collections.shuffle(list);
                // n_iterations++;
            } while (!isSolvable(list));

            // System.out.println("Puzzle generated in " + n_iterations + " iterations");

            // Setup grid adapter
            GridItemAdapter adapter = new GridItemAdapter(this, list, current_grid_size * current_grid_size - 1);
            grid_view.setAdapter(adapter);

            final boolean[] complete = {false};
            final int[] attempts = {0};

            grid_view.setOnTouchListener((View.OnTouchListener) (v, me) -> {

                if (me.getActionMasked() == ACTION_UP) {

                    if(complete[0]) {
                        return false;
                    }

                    // Get the clicked cell
                    int position = grid_view.pointToPosition((int) me.getX(), (int) me.getY());
                    GridItem gi = (GridItem) grid_view.getItemAtPosition(position);

                    if (gi != null) {
                        // Do not allow clicking on the free cell
                        if (gi.getI() == current_grid_size * current_grid_size - 1) {
                            return false;
                        }

                        // Find x/y of clicked item
                        int index = list.indexOf(gi);
                        int x = index % current_grid_size;
                        int y = (int) Math.floor(index / current_grid_size);
                        // System.out.println("Clicked cell: x " + x + " y " + y);

                        // Find x/y of free cell
                        int free_index = 0;

                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getI() == current_grid_size * current_grid_size - 1) {
                                free_index = i;
                                break;
                            }
                        }

                        int free_x = free_index % current_grid_size;
                        int free_y = (int) Math.floor(free_index / current_grid_size);
                        // System.out.println("Free cell x " + free_x+ " y " + free_y);

                        // Check if we can mode it up/down/left/right
                        boolean can_move = (free_x == x && Math.abs(free_y - y) == 1) || (free_y == y && Math.abs(free_x - x) == 1);
                        // System.out.println("can move " + can_move);

                        if (can_move) {
                            attempts[0]++;

                            list.set(index, list.get(free_index));
                            list.set(free_index, gi);

                            // Check complete
                            boolean resolved = true;

                            for (int i = 1; i < list.size(); i++) {
                                if (list.get(i - 1).getI() > list.get(i).getI()) {
                                    resolved = false;
                                    break;
                                }
                            }

                            complete[0] = resolved;

                            // Make the grid reveal the image
                            if(resolved) {
                                adapter.reveal();
                                Toast.makeText(this, "Puzzle solved in "  + attempts[0] +" moves", Toast.LENGTH_LONG).show();
                            }

                            adapter.notifyDataSetChanged();
                            // System.out.println("Resovled " + resolved);
                        }

                    }
                }
                return false;
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}